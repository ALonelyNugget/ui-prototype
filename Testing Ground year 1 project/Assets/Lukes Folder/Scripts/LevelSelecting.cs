﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelecting : MonoBehaviour
{
    public GameObject openMap;
    static public bool mapOpen;
    // Start is called before the first frame update
    void Start()
    {

        
        openMap.SetActive(false);
        

    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    public void PauseGame()
    {
        if (PauseMenu.isPaused == false)
        {
            openMap.SetActive(true);
            Time.timeScale = 0f;
            mapOpen = true;
        }
    }
    public void ResumeGame()
    {
        if (PauseMenu.isPaused == false)
        {
            openMap.SetActive(false);
            Time.timeScale = 1f;
            mapOpen = false;
        }

    }
    public void Level1()
    {

        SceneManager.LoadScene("Battle1Sample");
    }
    public void Level2()
    {
        SceneManager.LoadScene("Battle2Sample");
    }
    public void Level3()
    {
        SceneManager.LoadScene("Battle3Sample");
    }
    public void LevelFinal()
    {
        SceneManager.LoadScene("FinalBattleSample");
    }
}
