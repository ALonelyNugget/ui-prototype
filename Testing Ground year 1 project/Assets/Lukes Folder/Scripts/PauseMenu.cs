﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    static public bool isPaused;
    // Start is called before the first frame update
    void Start()
    {
        

        pauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {


    }



    public void PauseGame()
    {
        if (LevelSelecting.mapOpen == false)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            isPaused = true;
        }
    }
    public void ResumeGame()
    {
        if (LevelSelecting.mapOpen == false)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
            isPaused = false;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
